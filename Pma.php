<?php

class Pma {

	private $url = 'https://www.phpmyadmin.net/home_page/version.json';
	private $file = 'phpmyadmin.json';
	private $zip;
	private $info;


	public function run() {
		$this->compareVersions();
	}

	private function downloadFile() {
		$this->getInfo();
		$version = $this->info->version;
		$url = 'https://files.phpmyadmin.net/phpMyAdmin/'.$version.'/phpMyAdmin-'.$version.'-all-languages.zip';
		$this->zip = $name = basename($url);
		$content = $this->getContent($url);
		$file = file_put_contents($name, $content);
		$this->unZip($this->zip);
	}

	private function unZip() {
		$zip = new ZipArchive;
		$res = $zip->open($this->zip);
		if($res === TRUE) {
			$zip->extractTo('./');
			$zip->close();
			$this->configEdit();
		}
	}

	private function DirZip($source, $destination) {
		if(!extension_loaded('zip') || !file_exists($source)) {
			return false;
		}
		$zip = new ZipArchive();
		if(!$zip->open($destination, ZIPARCHIVE::CREATE)) {
			return false;
		}
		$source = str_replace('\\', DIRECTORY_SEPARATOR, realpath($source));
		if(is_dir($source) === true) {
		  $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($source), RecursiveIteratorIterator::SELF_FIRST);
		  foreach($files as $file) {
				$file = str_replace('\\', DIRECTORY_SEPARATOR, $file);
				if(in_array(substr($file, strrpos($file, DIRECTORY_SEPARATOR)+1), array('.', '..')))
				continue;
				$file = realpath($file);
				if(is_dir($file) === true) {
					$zip->addEmptyDir(str_replace($source . DIRECTORY_SEPARATOR, '', $file . DIRECTORY_SEPARATOR));
				}elseif(is_file($file) === true) {
					$zip->addFromString(str_replace($source . DIRECTORY_SEPARATOR, '', $file), file_get_contents($file));
				}
			}
		}elseif(is_file($source) === true){
			$zip->addFromString(basename($source), file_get_contents($source));
		}
		if($zip->close()) {
			unlink($this->zip);
			if($this->removeDir($source)){
				echo "Task Completed";
			}
		}
	}

	private function configEdit() {
		$dir = $this->fileToDir();
		$blowfish = hash('tiger128,4', date("hisa"));
		$old_lines = [
			'$cfg[\'Servers\'][$i][\'AllowNoPassword\'] = false;',
			'$cfg[\'blowfish_secret\'] = \'\'; /* YOU MUST FILL IN THIS FOR COOKIE AUTH! */',
		];
		$new_lines = [
			'$cfg[\'Servers\'][$i][\'AllowNoPassword\'] = true;',
			'$cfg[\'blowfish_secret\'] = \''.$blowfish.'\';',
		];
		$str = file_get_contents($dir.'/config.sample.inc.php');
		$str = str_replace($old_lines, $new_lines, $str);
		if(file_put_contents($dir.'/config.inc.php', $str)) {
			unlink($dir.'/config.sample.inc.php');
			chmod($dir, 0755);
			$new_name = 'phpMyAdmin';
			rename($dir, $new_name);
			$this->DirZip($new_name, $new_name.'-'.$this->info->version.'.zip');
		}
	}

	private function fileToDir() {
		$info = pathinfo($this->zip);
		return $info['filename'];
	}

	private function compareVersions() {
		if($this->getInfo()) {
			if(!file_exists('phpMyAdmin-'.$this->info->version.'.zip')){
				$this->downloadFile();
				$this->saveInfo($this->info);
				return true;
			}elseif($this->savedInfo()->version != $this->info->version) {
				$this->downloadFile();
				$this->saveInfo($this->info);
				return true;
			}
			echo "Your phpMyAdmin is uptodate";
		}
	}

	private function getInfo() {
		if($this->checkUrl()) {
			$json = $this->getContent($this->url);
			$this->info = json_decode($json);
			if(!file_exists($this->file)) {
				$this->saveInfo($this->info);
			}
			return true;
		}else{
			echo "Unable to reach the PMA Server";
		}
	}

	private function saveInfo($info) {
		$fp = fopen($this->file, 'w');
		fwrite($fp, json_encode($info, JSON_PRETTY_PRINT));
		fclose($fp);
	}

	private function savedInfo() {
		if(file_exists($this->file)) {
			$json = file_get_contents($this->file);
			if($json == NULL) {
				$this->getInfo();
				$this->saveInfo($this->info);
			}
			return json_decode($json);
		}
	}

	private function getContent($url) {
		$file_get_options = array(
			"ssl" => array(
				"verify_peer" => false,
				"verify_peer_name" => false,
			),
		);
		$content = file_get_contents($url, false, stream_context_create($file_get_options));
		return $content;
	}

	private function checkUrl() {
		$curl = curl_init();
		$options = array(
			CURLOPT_URL => $this->url,
			CURLOPT_SSL_VERIFYPEER => false,
			CURLOPT_SSL_VERIFYHOST => false,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HEADER => true,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_ENCODING => "",
			CURLOPT_AUTOREFERER => true,
			CURLOPT_CONNECTTIMEOUT => 120,
			CURLOPT_TIMEOUT => 120,
			CURLOPT_MAXREDIRS => 10
		);
		curl_setopt_array( $curl, $options);
		$response = curl_exec($curl);
		$httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		$response = curl_exec($curl);
		curl_close($curl);
		if($httpcode == 200) {
			return true;
		}else{
			return false;
		}
	}

	private function removeDir($path) {
		$dir = new DirectoryIterator($path);
		foreach($dir as $fileinfo) {
			if ($fileinfo->isFile() || $fileinfo->isLink()) {
				unlink($fileinfo->getPathName());
			}
			elseif (!$fileinfo->isDot() && $fileinfo->isDir()) {
				$this->removeDir($fileinfo->getPathName());
			}
		}
		rmdir($path);
		return true;
	}

}


?>
